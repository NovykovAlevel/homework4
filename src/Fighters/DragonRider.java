package Fighters;

public class DragonRider extends ArenaFighters {
    private Dragon ridingDragon;
    private float healthDragon;

    public DragonRider(String name, float health, float damage, float armor) {
        super(name, health, damage, armor);
    }

    @Override
    public void attack(ArenaFighters opponent) {
        if (opponent instanceof Dragon) {
            attackDragon((Dragon) opponent);
        } else {
            opponent.damaged(this.damage);
        }
    }

    private void attackDragon(Dragon dragon) {
        this.ridingDragon = dragon;
        healthDragon = dragon.getHealth();
        dragon.setHealth(0);
    }
}
