package Fighters;

public class HollyKnight extends Knight{
    private float recovery;

    public HollyKnight(String name, float health, float damage, float armor, float shield, float recovery) {
        super(name, health, damage, armor, shield);
        this.recovery = recovery;
    }

    private void recovery() {
        if (this.isAlfie()) {
            this.heal(recovery);
            System.out.println(this.getName() + " recovery himself by " + recovery + " hp");
        }
    }

    @Override
    public void attack(ArenaFighters arenaFighter) {
        super.attack(arenaFighter);
        recovery();
    }
}
